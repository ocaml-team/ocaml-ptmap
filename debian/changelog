ocaml-ptmap (2.0.5-5) unstable; urgency=medium

  * Fix d/watch.
  * Bump Standards-Version to 4.7.0 (no changes).

 -- Andy Li <andy@onthewings.net>  Sun, 09 Feb 2025 06:48:27 +0000

ocaml-ptmap (2.0.5-4) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Depend on ocaml instead of transitional ocaml-nox

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.6.0, no changes needed.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 02 Sep 2023 09:53:32 +0200

ocaml-ptmap (2.0.5-3) unstable; urgency=medium

  * Team upload
  * Include ocamlvars.mk in debian/rules

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2023 06:43:59 +0200

ocaml-ptmap (2.0.5-2) unstable; urgency=medium

  * Team upload
  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Sat, 15 Jul 2023 16:01:32 +0200

ocaml-ptmap (2.0.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update debian/watch to point to upstream .tbz file
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Wed, 14 Oct 2020 05:40:43 +0200

ocaml-ptmap (2.0.4-3) unstable; urgency=medium

  * Remove ocaml-4.05.patch to build with ocaml 4.08.
  * Bump Standards-Version to 4.4.1 (no changes).

 -- Andy Li <andy@onthewings.net>  Sun, 10 Nov 2019 11:02:04 +0800

ocaml-ptmap (2.0.4-2) unstable; urgency=medium

  * Disable test since the test isn't compatible with recent version of qcheck.
    (Closes: #935617)
  * Bump Standards-Version to 4.4.0 (no changes).
  * Mark libptmap-ocaml-doc Multi-Arch: foreign.

 -- Andy Li <andy@onthewings.net>  Sun, 25 Aug 2019 16:41:56 +0800

ocaml-ptmap (2.0.4-1) unstable; urgency=medium

  * Initial release. (Closes: #913456)
  * Add ocaml-4.05.patch that removes refs to things in OCaml 4.06+.

 -- Andy Li <andy@onthewings.net>  Sun, 02 Dec 2018 22:09:15 +0800
